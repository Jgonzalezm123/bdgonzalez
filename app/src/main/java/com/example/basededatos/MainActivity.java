package com.example.basededatos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText et_codigo, et_nombre, et_categoria, et_protagonistas, et_año, et_numerot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_codigo =(EditText)findViewById(R.id.txt_nombre);
        et_nombre = (EditText)findViewById(R.id.txt_nombreserie);
        et_categoria = (EditText)findViewById(R.id.txt_categoria);
        et_protagonistas=(EditText)findViewById(R.id.txt_protagonistas);
        et_año=(EditText)findViewById(R.id.txt_año);
        et_numerot=(EditText)findViewById(R.id.txt_numerot);
    }

    public void Registrar (View view){


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase basededatos = admin.getWritableDatabase();

        String codigo = et_codigo.getText().toString();
        String nombre = et_nombre.getText().toString();
        String categoria= et_categoria.getText().toString();
        String protagonistas = et_protagonistas.getText().toString();
        String año = et_año.getText().toString();
        String numerot = et_numerot.getText().toString();

        if(!codigo.isEmpty() && !nombre.isEmpty() && !categoria.isEmpty() && !protagonistas.isEmpty() && !año.isEmpty() && !numerot.isEmpty()){

            ContentValues registro = new ContentValues();
            registro.put("codigo", codigo);
            registro.put("nombre", nombre);
            registro.put("categoria", categoria);
            registro.put("protagonistas", protagonistas);
            registro.put("año", año);
            registro.put("numerot", numerot);

            basededatos.insert("series", null, registro);
            basededatos.close();
            et_codigo.setText("");
            et_nombre.setText("");
            et_categoria.setText("");
            et_protagonistas.setText("");
            et_año.setText("");
            et_numerot.setText("");

            Toast.makeText(this,"Registro exitoso", Toast.LENGTH_SHORT).show();


        } else{


            Toast.makeText(this,"Debe de llenar los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void Buscar (View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase basededatos = admin.getWritableDatabase();
        String codigo = et_codigo.getText().toString();

        if (!codigo.isEmpty()){

            Cursor fila = basededatos.rawQuery
                    ("select nombre, categoria, protagonistas, año, numerot from series where codigo =" + codigo, null);

            if(fila.moveToFirst()){
                et_nombre.setText(fila.getString(0));
                et_categoria.setText(fila.getString(1));
                et_protagonistas.setText(fila.getString(2));
                et_año.setText(fila.getString(3));
                et_numerot.setText(fila.getString(4));
                basededatos.close();

            }
            else {

                Toast.makeText(this, "La serie no existe", Toast.LENGTH_SHORT).show();
                basededatos.close();
            }

        }else{

        Toast.makeText(this, "Debes introducir el codigo de la serie", Toast.LENGTH_SHORT).show();
        }


    }

    public void Eliminar(View view){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase basededatos = admin.getWritableDatabase();
            String codigo = et_codigo.getText().toString();

            if (!codigo.isEmpty()){

                int cantidad  = basededatos.delete("series", "codigo = " + codigo,null );
                basededatos.close();
                et_codigo.setText("");
                et_nombre.setText("");
                et_categoria.setText("");
                et_protagonistas.setText("");
                et_año.setText("");
                et_numerot.setText("");

                if(cantidad == 1){

                    Toast.makeText(this,"Serie eliminada", Toast.LENGTH_SHORT).show();

                }else{

                    Toast.makeText(this,"La serie no existe", Toast.LENGTH_SHORT).show();
                }

            } else{

                Toast.makeText(this, "Debes introducir el codigo de la serie", Toast.LENGTH_SHORT).show();
            }

    }

    public void Modificar(View view){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,"administracion", null, 1);
        SQLiteDatabase basededatos = admin.getWritableDatabase();

        String codigo = et_codigo.getText().toString();
        String nombre = et_nombre.getText().toString();
        String categoria = et_categoria.getText().toString();
        String protagonistas = et_protagonistas.getText().toString();
        String año = et_año.getText().toString();
        String numerot = et_numerot.getText().toString();

        if(!codigo.isEmpty() && !nombre.isEmpty() && !categoria.isEmpty() && !protagonistas.isEmpty() && !año.isEmpty() && !numerot.isEmpty()){

            ContentValues registro = new ContentValues();
            registro.put("codigo", codigo);
            registro.put("nombre", nombre);
            registro.put("categoria", categoria);
            registro.put("protagonistas", protagonistas);
            registro.put("año", año);
            registro.put("numerot", numerot);

            int cantidad = basededatos.update("series ", registro, "codigo =" +codigo, null);
            basededatos.close();

            if(cantidad == 1){
                Toast.makeText(this,"Serie modificada correctamente", Toast.LENGTH_SHORT).show();


            } else{

                Toast.makeText(this,"No existe la serie", Toast.LENGTH_SHORT).show();

            }
        }else{

            Toast.makeText(this,"Debes de llenar todos los campos", Toast.LENGTH_SHORT).show();
        }

    }
}
